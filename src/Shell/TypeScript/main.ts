﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/amplifyjs/amplifyjs.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />

module shell {
    interface shellMessage {
        topic: string;
        input: any;
    };

    module log {
        export function rcvd(subscriber: string, input: any) {
            console.log("[shell-rcvd] amplifySub (" + subscriber + ") => " + ko.toJSON(input));
        }
    }

    module core {
        // TODO: Enhance callback argument with an interface to enhance readability.
        export function setup(topic: string, callback: Function): void {
            amplify.subscribe(topic, (input: any) => {
                log.rcvd(topic, input);
                callback(input);
            });
        }
    }

    module ui {
        var buttonPublisher: string = "urn:web-core-ui-button-pub";

        export function listen() {
            onmessage = (e: MessageEvent) => {
                var message = <shellMessage>e.data;
                amplify.publish(message.topic, message.input);
            };
        }

        export function button(input: any) {
            var control = $("<button></button>");
            control.text(input.text);
            $(".js-ui-pane").append(control);
        }

        core.setup(buttonPublisher, button);
    }

    ui.listen();
}

module main {
    //var systemTopic: string = "urn:web-core-net";

    //amplify.subscribe(systemTopic, (message: any) => {
    //    console.log("[rcvd]: amplifySub (" + systemTopic + ") => " + ko.toJSON(message));
    //});

    //onmessage = (e: MessageEvent) => {
    //    console.log("[rcvd] onmessage (origin) => " + e.origin);
    //    console.log("[rcvd] onmessage (data) => " + ko.toJSON(e.data));
    //    amplify.publish(systemTopic, e.data);
    //};
}