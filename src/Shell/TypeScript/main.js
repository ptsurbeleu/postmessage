﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/amplifyjs/amplifyjs.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
var shell;
(function (shell) {
    ;

    var log;
    (function (log) {
        function rcvd(subscriber, input) {
            console.log("[shell-rcvd] amplifySub (" + subscriber + ") => " + ko.toJSON(input));
        }
        log.rcvd = rcvd;
    })(log || (log = {}));

    var core;
    (function (core) {
        // TODO: Enhance callback argument with an interface to enhance readability.
        function setup(topic, callback) {
            amplify.subscribe(topic, function (input) {
                log.rcvd(topic, input);
                callback(input);
            });
        }
        core.setup = setup;
    })(core || (core = {}));

    var ui;
    (function (ui) {
        var buttonPublisher = "urn:web-core-ui-button-pub";

        function listen() {
            onmessage = function (e) {
                var message = e.data;
                amplify.publish(message.topic, message.input);
            };
        }
        ui.listen = listen;

        function button(input) {
            var control = $("<button></button>");
            control.text(input.text);
            $(".js-ui-pane").append(control);
        }
        ui.button = button;

        core.setup(buttonPublisher, button);
    })(ui || (ui = {}));

    ui.listen();
})(shell || (shell = {}));
//# sourceMappingURL=main.js.map
