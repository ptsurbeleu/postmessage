﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/amplifyjs/amplifyjs.d.ts" />
var shell;
(function (shell) {
    var log;
    (function (log) {
        function rcvd(subscriber, input) {
            console.log("[modulea-rcvd] amplifySub (" + subscriber + ") => " + ko.toJSON(input));
        }
        log.rcvd = rcvd;
    })(log || (log = {}));

    var core;
    (function (core) {
        // TODO: Figure out how to setup message exchange details in the code
        var shellOrigin = "http://localhost:1174/";

        function setup(topic) {
            amplify.subscribe(topic, function (input) {
                parent.postMessage({ topic: topic, input: input }, shellOrigin);
            });
        }
        core.setup = setup;
    })(core || (core = {}));

    (function (ui) {
        var buttonPublisher = "urn:web-core-ui-button-pub";

        function button(text) {
            amplify.publish(buttonPublisher, { text: text });
        }
        ui.button = button;

        core.setup(buttonPublisher);
    })(shell.ui || (shell.ui = {}));
    var ui = shell.ui;
})(shell || (shell = {}));

var main;
(function (main) {
    shell.ui.button("Yes-1");
    shell.ui.button("Yes-2");
})(main || (main = {}));
//# sourceMappingURL=main.js.map
