﻿/// <reference path="../scripts/typings/jquery/jquery.d.ts" />
/// <reference path="../scripts/typings/knockout/knockout.d.ts" />
/// <reference path="../scripts/typings/amplifyjs/amplifyjs.d.ts" />

module shell {
    module log {
        export function rcvd(subscriber: string, input: any) {
            console.log("[modulea-rcvd] amplifySub (" + subscriber + ") => " + ko.toJSON(input));
        }
    }

    module core {
        // TODO: Figure out how to setup message exchange details in the code
        var shellOrigin: string = "http://localhost:1174/";

        export function setup(topic: string): void {
            amplify.subscribe(topic, (input: any) => {
                parent.postMessage({ topic: topic, input: input }, shellOrigin);
            });
        }
    }

    export module ui {
        var buttonPublisher: string = "urn:web-core-ui-button-pub";

        export function button(text: string) {
            amplify.publish(buttonPublisher, { text: text });
        }

        core.setup(buttonPublisher);
    }
}

module main {
    shell.ui.button("Yes-1");
    shell.ui.button("Yes-2");
}